Persona.Router.map(function() {
  this.route('index', { path: '/'});
  
  this.resource('outfits');
});

Persona.IndexRoute = Ember.Route.extend({
  redirect: function() {
    this.transitionTo('outfits');
  }
});

Persona.OutfitsRoute = Ember.Route.extend({
  setupController: function(controller) {
    controller.set('content', Persona.Outfit.find())
  } 
});
