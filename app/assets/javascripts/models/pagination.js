Persona.Pagination = DS.Model.extend({
  page: DS.attr('number'),
  totalPages: DS.attr('number'),
  totalItems: DS.attr('number'),
  perPage: DS.attr('number')
});
