Persona.Outfit = DS.Model.extend({
  moderatedAt: DS.attr('date'),
  contributor: DS.attr('contributor'),
  loves: DS.attr('loves'),
  hasLoved: DS.attr('boolean'),
  images: DS.attr('images'),

  loveCount: function() {
    return this.get('loves').count;
  }.property('loves').cacheable(),

  imageUrl: function() {
    return this.get('images').originalImage.url
  }.property('images').cacheable()

});

