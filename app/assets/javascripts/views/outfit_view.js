Persona.OutfitView = Ember.View.extend({
  templateName: 'outfit',
  tagName: 'li',
  loveButtonValue: 'Love',

  toggleLove: function() {
    var outfit = this.get('outfit'); // don't understand this
    // console.log('outfit', outfit);
    // console.log('this', this);
    outfit.set( 'hasLoved', !outfit.get('hasLoved') );
    
    // update button state
    // TODO - do this with computed property?
    
    var btnLabel = 'Love';
    if (outfit.get('hasLoved')) {
      btnLabel = 'Unlove';
    }
    this.set('loveButtonValue', btnLabel);
  },

  // TODO - this doesn't get updated on the view when outfit changes
  // probably because it's a different outfit instance
  xloveButtonValue: function() {
    var outfit = this.get('outfit'); // don't understand this
    var btnLabel = 'Love';
    if (outfit.get('hasLoved')) {
      btnLabel = 'Unlove';
    }

    return btnLabel;
  }.property('hasLoved').cacheable()
  
});
