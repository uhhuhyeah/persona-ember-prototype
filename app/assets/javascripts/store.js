var PersonaAdapter = DS.RESTAdapter.extend();

PersonaAdapter.configure('plurals', {
  pagination: 'pagination'
});

PersonaAdapter.configure('Persona.Pagination', {sideloadAs: 'pagination'});


// Hacks for hasOne
PersonaAdapter.registerTransform('contributor', {
  serialize: function(value) {
    return [value.id, value.name, value.personal_website_url]; // TODO - this probably should be an object
  },
  deserialize: function(value) {
    return Ember.create({ id: value.id, name: value.name, personalWebsiteUrl: value.personal_website_url });
  }
});

PersonaAdapter.registerTransform('loves', {
  serialize: function(value) {
    return [value.count, value.accountIds]; // TODO - this probably should be an object
  },
  deserialize: function(value) {
    return Ember.create({ count: value.count, accountIds: value.account_ids} );
  }
});

PersonaAdapter.registerTransform('images', {
  serialize: function(value) {
    return []; // TODO - this probably should be an object
  },
  deserialize: function(value) {
    var o, m, m2, l, l2;
    o = value.original;
    m = value.medium;
    m2 = value.medium2x;
    l = value.large;
    l2 = value.large2x;

    var originalImage = Ember.create({ url: o.url, width: o.width, height: o.height });

    return { originalImage: originalImage };
  }
});

Persona.Store = DS.Store.extend({
  revision: 11,
  adapter: PersonaAdapter.create({
    url: 'http://community.demo.modcloth.com/',
    namespace: 'style-gallery/api/v1'
  })
});
